
// Ezam 2 Practical
// Simon Schick

#include <iostream>
#include <conio.h>
#include <fstream>

using namespace std;

double FindAverage(double numbInputs[], int size)
{
	double sum = 0;
	for (int i = 0; i < size; i++)
		sum += numbInputs[i];

	return sum/5;
}

void PrintToFile(double numbInputs[], double min, double max, int size, ostream &out) // both function have the size as aparameter so the size isn't hardcoded incase if I decide to change the size to like 12
{
	for (int i = 0; i < size; i++)
		out << numbInputs[i] << "\n";
	out << "Average: " << FindAverage(numbInputs, size) << "\nMin: " << min << "\nMax: " << max;
}

int main()
{
	const int SIZE = 5;
	double numbers[SIZE];
	double min = 0;
	double max = 0;
	bool firstInput = true;
	char save;

	string filePath = "numbersAvgMinMax.txt";
	

	cout << "Please enter in 5 numbers.";
	
	for(int i =0; i < SIZE; i++)
	{
		cout << "\nNumber " << (i + 1) << ": ";
		cin >> numbers[i];
		if(firstInput == true) // using a bool to set the min and max to the first input
		{
			firstInput = false;
			min = numbers[i];
			max = numbers[i];
		}
		else // used to see if current input is greater or less than the min and max stored in memory
		{
			if (numbers[i] < min)
				min = numbers[i];

			if (numbers[i] > max)
				max = numbers[i];
		}
	}
	cout << "\nYour inputs are \n";

	for (int i = 0; i < SIZE; i++)
		cout << numbers[i] << "\n";

	cout << "\nThe average of your inputs are: " << FindAverage(numbers, SIZE) << "\nAnd the minimum is: " << min << "\nAnd your maximum is: " << max; // I hope what I did doesn't count as the function printing to the console or file

	cout << "\n\nWould you like to save your numbers? (Enter in a 'y' for yes, and anything else for no) ";
	cin >> save;
	if (save == 'Y' || save == 'y')
	{
		ofstream ofs(filePath);
		if (ofs)
		{
			PrintToFile(numbers, min, max, SIZE, ofs);
			cout << "Saved! Press any key to close the program";
		}
		ofs.close(); // prevents memory leaks
	}
	else
		cout << "Your numbers weren't saved. Press any key to close the program\n";

	(void)_getch();
	return 0;
}
